# Simple Oracle Pool

This library provides an interface to simply create an Oracle UCP connection pool. It also provides a servlet listener that will clean up the junk this driver leaves behind when you undeploy your application. This means no more memory leaks when using the Oracle JDBC driver. Yay!

## Installation

Clone this project, adjust the `distribution.properties` to match your Maven repositories details, and issue a `mvn deploy`. You can then add the following dependency to your project:

    <dependency>
      <groupId>edu.clayton</groupId>
      <artifactId>SimpleOraclePool</artifactId>
      <version>RELEASE</version>
    </dependency>

Note, you should have the Oracle JDBC [11.2.0.4](http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-112010-090769.html) and Oracle UCP [11.2.0.4](http://www.oracle.com/technetwork/database/enterprise-edition/downloads/ucp-112010-099129.html) drivers in your project dependencies. I have have them in my local Maven repository with the following dependency details:

    <dependency>
      <groupId>com.oracle</groupId>
      <artifactId>ojdbc16</artifactId>
      <version>11.2.0.4</version>
    </dependency>
    <dependency>
      <groupId>com.oracle</groupId>
      <artifactId>ucp</artifactId>
      <version>11.2.0.4</version>
    </dependency>

The **SimpleOraclePool** library is available via the following [Maven](http://maven.apache.org/) [repository](https://bitbucket.org/csu_jsumners/public-maven-repo/src/master/):

    <repository>
	  <id>csu-bitbucket-public</id>
      <url>https://bitbucket.org/csu_jsumners/public-maven-repo/src/master/</url>
	</repository>

## Usage

Let's pretend you are using [Spring](http://spring.io/) 3.2 or later, and are configuring your application through the [AbstractAnnotationConfigDispatcherServletInitializer](http://docs.spring.io/spring/docs/3.2.7.BUILD-SNAPSHOT/javadoc-api/org/springframework/web/servlet/support/AbstractAnnotationConfigDispatcherServletInitializer.html). First, you would implement the following method:

    @Override
    public void onStartup(ServletContext context) throws ServletException {
      context.addListener(new OjdbcDriverListener("my-connection-pool");
    }

You would then create a bean in your application like so:

    @Bean
    public PoolDataSource poolDataSource() {
      SimpleOraclePool simplePool = new SimpleOraclePool(
        "jdbc:oracle:thin:scott/tiger@mydb.host:1521:my_sid",
        "my-connection-pool"
      );
      return simplePool.getPoolDataSource();
    }

Notice that in each of these methods there is a string `"my-connection-pool"`. This string names the pool that your application will be using. It _must_ be the same name in both places. The `OjdbcDriverListener` will use that name to clean up the pool on context destruction.

Once you have the `PoolDataSource` created, you can use it like you normally would. For example, if you're using Spring JDBC, you could do:

    @Bean
    public JdbcTemplate jdbcTemplate() {
      return new JdbcTemplate(this.poolDataSource());
    }

Provided both beans are defined in the same class.
