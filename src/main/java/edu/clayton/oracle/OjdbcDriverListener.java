package edu.clayton.oracle;

import java.lang.management.ManagementFactory;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.management.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import oracle.jdbc.OracleDriver;
import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>This class provides a {@link javax.servlet.ServletContextListener} that
 * loads the Oracle JDBC driver on context initialization, and cleans up the
 * memory leaking objects it leaves behind on context destruction.</p>
 *
 * <p>Notes:</p>
 * <ol>
 *   <li>
 *     This is targeted toward the UCP driver version 11.2. See
 *     <a href="http://www.oracle.com/technetwork/database/enterprise-edition/downloads/ucp-112010-099129.html">
 *       http://www.oracle.com/technetwork/database/enterprise-edition/downloads/ucp-112010-099129.html
 *     </a> and
 *     <a href="http://docs.oracle.com/cd/E11882_01/java.112/e12265/toc.htm">
 *       http://docs.oracle.com/cd/E11882_01/java.112/e12265/toc.htm
 *     </a>
 *   </li>
 *   <li>
 *     You should add this listener as soon as possible and before other
 *     listeners.
 *   </li>
 *   <li>
 *     You should supply a pool name. A pool name is required to successfully
 *     unload all of the crap these drivers leave behind. The default constructor
 *     for this class will set the pool name to "ucp_pool".
 *   </li>
 *   <li>
 *     This is designed with only one pool in mind. If you're using multiple
 *     pools in your application, you would be better off doing all of this
 *     yourself.
 *   </li>
 * </ol>
 */
public class OjdbcDriverListener implements ServletContextListener {
  private static final Logger log = LoggerFactory.getLogger(OjdbcDriverListener.class);

  private Driver driver;
  private String poolName;

  /**
   * This will set the pool name to "ucp_pool". You probably want to use
   * {@link #OjdbcDriverListener(String)} instead.
   */
  public OjdbcDriverListener() {
    this("ucp_pool");
  }

  /**
   * Create an instance of the listener and set the pool name to {@code poolName}.
   *
   * @param poolName This is the name of pool that the listener will try to unload
   *                 when the context is destroyed. See
   *                 {@link oracle.ucp.jdbc.PoolDataSource#setConnectionPoolName(String)}
   */
  public OjdbcDriverListener(String poolName) {
    log.info("Creating Oralce driver listener for pool named `{}`", poolName);
    this.poolName = poolName;
  }

  public void contextInitialized(ServletContextEvent sce) {
    this.driver = new OracleDriver();
    boolean skipRegistration = false;
    Enumeration<Driver> loadedDrivers = DriverManager.getDrivers();

    log.info("Inspecting registered SQL drivers");
    while (loadedDrivers.hasMoreElements()) {
      Driver driver = loadedDrivers.nextElement();
      if (driver instanceof OracleDriver) {
        OracleDriver registeredDriver = (OracleDriver) driver;
        if (registeredDriver.getClass() == this.driver.getClass()) {
          log.info("Oracle driver already registered");
          skipRegistration = true;
          this.driver = registeredDriver;
          break;
        }
      }
    }

    if (!skipRegistration) {
      log.info("Attempting to register Oracle driver");
      try {
        DriverManager.registerDriver(this.driver);
        log.info(
          "Registered Oracle driver: `{} v{}.{}`",
          this.driver,
          this.driver.getMajorVersion(),
          this.driver.getMinorVersion()
        );
      } catch (SQLException e) {
        log.error("Could not register Oracle driver: `{}`", e.getMessage());
        log.debug(e.toString());
        throw new RuntimeException(e);
      }
    }
  }

  public void contextDestroyed(ServletContextEvent sce) {
    if (this.driver != null) {
      log.info("Attempting to unload Oracle garbage");

      try {
        log.info("Destroying connection pool named `{}`", this.poolName);
        UniversalConnectionPoolManager mgr = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
        mgr.destroyConnectionPool(this.poolName);

        log.info("Removing Oracle MBeans");
        this.killMBeans();

        log.info("Deregistering JDBC driver: `{}`", this.driver);
        DriverManager.deregisterDriver(this.driver);
        this.driver = null;
      } catch (UniversalConnectionPoolException e) {
        log.error("Could not acquire connection pool manager: `{}`", e.getMessage());
        log.debug(e.toString());
      } catch (SQLException e) {
        log.error("Could not deregister JDBC driver: `{}`", e.getMessage());
        log.debug(e.toString());
      }
    } else {
      log.info("No Oracle mess to cleanup");
    }
  }

  private void killMBeans() {
    final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

    for (ObjectName mbean : mbs.queryNames(null, null)) {
      final Hashtable<String, String> keys = new Hashtable<>(0);
      final String mbeanName = mbean.getCanonicalName();
      final Hashtable<String, String> properties = mbean.getKeyPropertyList();

      if (mbeanName.startsWith("oracle.ucp.admin") ||
          mbeanName.startsWith("com.oracle.jdbc"))
      {
        log.debug("Unloading MBean: `{}`", mbeanName);
        keys.put("name", properties.get("name"));
        if (properties.keySet().contains("type")) {
          keys.put("type", properties.get("type"));
        }

        try {
          ObjectName objectName = new ObjectName(mbeanName.split(":")[0], keys);
          mbs.unregisterMBean(objectName);
        } catch (MalformedObjectNameException e) {
          log.error("Could not create MBean object name: `{}`", e.getMessage());
          log.debug(e.toString());
        } catch (InstanceNotFoundException e) {
          log.error("Could not find MBean instance: `{}`", e.getMessage());
          log.debug(e.toString());
        } catch (MBeanRegistrationException e) {
          log.error("MBean not registered: `{}`", e.getMessage());
          log.debug(e.toString());
        }
      }
    }
  }
}