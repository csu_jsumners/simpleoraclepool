package edu.clayton.oracle;

import java.sql.SQLException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>This class provides a convenient way to configured a {@link oracle.ucp.jdbc.PoolDataSource}.
 * This data source can then be used retrieve connections to the configured database.</p>
 *
 * <p>It is recommended that you use the {@link #SimpleOraclePool(String, String)}
 * constructor to initialize this object. You should also add an instance of
 * {@link edu.clayton.oracle.OjdbcDriverListener} to your context, using the
 * same {@code poolName} as you specify here. If you don't, be prepared for
 * memory leaks when you undeploy an application using this class.</p>
 */
public class SimpleOraclePool {
  private static final Logger log = LoggerFactory.getLogger(SimpleOraclePool.class);

  private final PoolDataSource poolDataSource = PoolDataSourceFactory.getPoolDataSource();
  private Properties poolProperties;

  /**
   * <p>Create an instance that connects to the specificed JDBC URL and sets
   * the connection pool name to the specified {@code poolName}. The specified
   * {@code connectionUrl} must be a valid JDBC URL that contains the following
   * components:</p>
   *
   * <ol>
   *   <li>protocol</li>
   *   <li>subprotocol</li>
   *   <li>driver type</li>
   *   <li>username and password</li>
   *   <li>hostname / dns</li>
   *   <li>port number (optional)</li>
   *   <li>sid (optional)</li>
   * </ol>
   *
   * <p>See
   * <a href="http://docs.oracle.com/cd/E11882_01/appdev.112/e13995/oracle/jdbc/OracleDriver.html">
   *   http://docs.oracle.com/cd/E11882_01/appdev.112/e13995/oracle/jdbc/OracleDriver.html
   * </a> for details on valid URLs. This article is also helpful --
   * <a href="http://www.public.iastate.edu/~java/docs/guide/jdbc/connection.doc.html">
   *   http://www.public.iastate.edu/~java/docs/guide/jdbc/connection.doc.html
   * </a>.</p>
   *
   * <p>Note that the following defaults will be specified when using this
   * constructor:</p>
   *
   * <ul>
   *   <li>Minimum pool size: 1</li>
   *   <li>Maximum pool size: 25</li>
   *   <li>Validate connection on borrow: true</li>
   *   <li>Connection factory class: "oracle.jdbc.pool.OracleDataSource"</li>
   * </ul>
   *
   * @param connectionUrl A valid JDBC URL with credentials.
   * @param poolName A name to give the connection pool. Should match what you
   *                 used for {@link edu.clayton.oracle.OjdbcDriverListener}.
   */
  public SimpleOraclePool(String connectionUrl, String poolName) {
    this.init(connectionUrl, poolName);
  }

  /**
   * <p>A convenience constructor that accepts a URL in the form
   * "jdbc:oracle:thin:@host:123:sid". This constructor will end up doing the
   * same work as {@link #SimpleOraclePool(String, String)}. You should read the
   * documentation for that constructor (and probably just use it instead).</p>
   *
   * @param username The username to connect to the database with.
   * @param password The password for the user.
   * @param connectionUrl A JDBC URL without credentials.
   * @param poolName A name to give the connection pool. Should match what you
   *                 used for {@link edu.clayton.oracle.OjdbcDriverListener}.
   */
  public SimpleOraclePool(String username, String password, String connectionUrl, String poolName) {
/*    this.poolProperties.setProperty(PoolDataSource.UCP_USER, username);
    this.poolProperties.setProperty(PoolDataSource.UCP_PASSWORD, password);*/
    String[] components = connectionUrl.split("@");
    String url = String.format("%s%s/%s@%s", components[0], username, password, components[1]);
    this.init(url, poolName);
  }

  /**
   * <p>A convenience method that allows you to specify all of the pool
   * properties yourself. You can use this if the defaults specified by
   * {@link #init(String, String)} are not sufficient for your needs.</p>
   *
   * <p>The properties you are allowed to set are defined by the constants in
   * <a href="http://docs.oracle.com/cd/E11882_01/java.112/e12826/oracle/ucp/jdbc/PoolDataSource.html">
   *   http://docs.oracle.com/cd/E11882_01/java.112/e12826/oracle/ucp/jdbc/PoolDataSource.html
   * </a>. At the very least, you should specify the following properties:</p>
   *
   * <ol>
   *   <li>{@link oracle.ucp.jdbc.PoolDataSource#UCP_CONNECTION_POOL_NAME}</li>
   *   <li>{@link oracle.ucp.jdbc.PoolDataSource#UCP_URL}</li>
   *   <li>
   *     {@link oracle.ucp.jdbc.PoolDataSource#UCP_VALIDATE_CONNECTION_ON_BORROW}
   *     (true)
   *   </li>
   *   <li>
   *     {@link oracle.ucp.jdbc.PoolDataSource#UCP_CONNECTION_FACTORY_CLASS_NAME}
   *     ("oracle.jdbc.pool.OracleDataSource")
   *   </li>
   * </ol>
   *
   * @param connectionProperties A valid connection pool {@link java.util.Properties} object.
   */
  public SimpleOraclePool(Properties connectionProperties) {
    this.poolProperties = poolProperties;
    this.setConnectionProperties();
  }

  /**
   * Retrieve the configured data source pool.
   *
   * @return An instance of {@link oracle.ucp.jdbc.PoolDataSource} with all
   * properties configured.
   */
  public PoolDataSource getPoolDataSource() {
    return this.poolDataSource;
  }

  private void init(String connectionUrl, String poolName) {
    this.poolProperties = new Properties();
    this.poolProperties.setProperty(PoolDataSource.UCP_CONNECTION_POOL_NAME, poolName);
    this.poolProperties.setProperty(PoolDataSource.UCP_URL, connectionUrl);
    this.poolProperties.setProperty(PoolDataSource.UCP_MIN_POOL_SIZE, "1");
    this.poolProperties.setProperty(PoolDataSource.UCP_MAX_POOL_SIZE, "25");
    this.poolProperties.setProperty(PoolDataSource.UCP_VALIDATE_CONNECTION_ON_BORROW, "true");
    this.poolProperties.setProperty(PoolDataSource.UCP_CONNECTION_FACTORY_CLASS_NAME, "oracle.jdbc.pool.OracleDataSource");

    Pattern pattern = Pattern.compile(".+:(.+)/(.+)@.*");
    Matcher matcher = pattern.matcher(connectionUrl);
    if (matcher.find()) {
      this.poolProperties.setProperty(PoolDataSource.UCP_USER, matcher.group(1));
      this.poolProperties.setProperty(PoolDataSource.UCP_PASSWORD, matcher.group(2));
    }

    this.setConnectionProperties();
  }

  private void setConnectionProperties() {
    try {
      // Of course the stupid driver wouldn't just accept a properties object
      // and do the right thing with it. Of course we get to do this
      // stupid shit. Fuck you Oracle.
      this.poolDataSource.setConnectionFactoryClassName(
        this.poolProperties.getProperty(PoolDataSource.UCP_CONNECTION_FACTORY_CLASS_NAME)
      );
      this.poolDataSource.setUser(
        this.poolProperties.getProperty(PoolDataSource.UCP_USER)
      );
      this.poolDataSource.setPassword(
        this.poolProperties.getProperty(PoolDataSource.UCP_PASSWORD)
      );
      this.poolDataSource.setURL(
        this.poolProperties.getProperty(PoolDataSource.UCP_URL)
      );
      this.poolDataSource.setConnectionPoolName(
        this.poolProperties.getProperty(PoolDataSource.UCP_CONNECTION_POOL_NAME)
      );

      String minPoolSizeString = this.poolProperties.getProperty(PoolDataSource.UCP_MIN_POOL_SIZE);
      int minPoolSize = (minPoolSizeString == null) ? 1 : Integer.parseInt(minPoolSizeString);
      this.poolDataSource.setMinPoolSize(minPoolSize);

      String maxPoolSizeString = this.poolProperties.getProperty(PoolDataSource.UCP_MAX_POOL_SIZE);
      int maxPoolSize = (maxPoolSizeString == null) ? 25 : Integer.parseInt(maxPoolSizeString);
      this.poolDataSource.setMaxPoolSize(maxPoolSize);

      String validateString = this.poolProperties.getProperty(PoolDataSource.UCP_VALIDATE_CONNECTION_ON_BORROW);
      boolean validate = (validateString == null) ? true : Boolean.parseBoolean(validateString);
      this.poolDataSource.setValidateConnectionOnBorrow(validate);

      this.poolDataSource.setConnectionProperties(this.poolProperties);
    } catch (SQLException e) {
      log.error("Could not set pool properties: `{}`", e.getMessage());
      log.debug(e.toString());
      throw new RuntimeException(e);
    }
  }
}